var cursorPosition = 0;
var interval;
var timerRunning = false;

// this tick function is called every 100 ms (10x per second)
function tick(){	
	// read current time from spans on page
	var tens = parseInt($('span#tens').text());
	var seconds = parseInt($('span#seconds').text());
	var minutes = parseInt($('span#minutes').text());
		
	// increasing "tens" counter
	if (tens < 9){
		tens++;
	}else{
		tens = 0;
		// increasing "seconds" counter
		if (seconds < 59){	
			seconds++;
		}else{
			seconds = 0;
			minutes++;
		}
	}
	
	if (seconds < 10){
		seconds = "0" + seconds;
	}
	
	// printing time from our variables to our spans on page
	$('span#tens').text(tens);
	$('span#seconds').text(seconds);
	$('span#minutes').text(minutes);
}

// starts the timer
function startTimerIfNotRunning(){
	if (timerRunning == false){
		startTimer();
		timerRunning = true;
	}
}

// starts timer
function startTimer(){
	interval = setInterval(tick, 100);
}

// stops timer
function stopTimer(){
	clearInterval(interval);
}

///////////// ^^ casove funkce ////////////////

// sends ajax request to server to record finished excercise to excercises table
function recordExercise(lesson, timeInSeconds, numberOfMistakes, charactersPerMinute){
	$.ajax({
		url: '/recordExercise?lesson=' + lesson + '&timeInSeconds=' + timeInSeconds + '&numberOfMistakes=' + numberOfMistakes+ '&charactersPerMinute=' + charactersPerMinute									
	})
}

// this function is called when 
function lectureFinished(){
	stopTimer();
	var numberOfMistakes = $('span.red').length; // this is how we count number of mistakes (number of spans with class "red")
	var cas = $('span#minutes').text() + ':' + $('span#seconds').text() + ':' + $('span#tens').text();
	var lesson = $('input#lesson').val();
	var timeInSeconds = ((parseInt($('span#minutes').text()) * 60) + parseInt($('span#seconds').text())) + '.' + $('span#tens').text();
	var numberOfCharacters = $('span.red').length + $('span.green').length;;
	var charactersPerMinute = Math.round((60 / timeInSeconds) * numberOfCharacters);
	recordExercise(lesson, timeInSeconds, numberOfMistakes, charactersPerMinute);
	alert('Congratulations! You finished lesson in time ' + cas + ' and you made ' + numberOfMistakes + ' mistakes! Your speed is ' + charactersPerMinute + ' CPM!');
}

// jquery functions - reaction to key press
$( document ).ready(function() {            	
	// this method is triggered every time key is pressed
	$(document).keypress(function(e){      						
		startTimerIfNotRunning();
		var keyPressed = String.fromCharCode(e.which);
		var lesson = $('input#lesson').val();
		
		console.log('Pressed key ' + keyPressed);  						
	
		// ajax call to check if pressed letter is correct
		$.ajax({
			url: '/checkLetter?letterCode=' + e.which + '&position=' + cursorPosition + '&lesson=' + lesson									
		}).done(function(data){
			console.log('Vracena odpoved: ' + JSON.stringify(data));								
			// if we received response "ok", it meants pressed key was correct and we shoudl mark letter green (add class "green")
			if (data.response == "ok"){
				$('#letterId' + cursorPosition).addClass('green');	
			}else{
				$('#letterId' + cursorPosition).addClass('red');	
			}
			
			// move "cursor" (| character)
			$('#cursor' + (cursorPosition + 1)).html('|');
			$('#cursor' + cursorPosition).html('');
			
			$('#handsImg').attr("src", "static/prsty/" + data.nextFinger + ".jpg");
			
			cursorPosition++;		
						
			if (data.finished == true){
				lectureFinished();
			}
		});						
	});
	
	$(document).keydown(function(e){      						
		if (e.which == 8){	// 8 == back space in ascii (to fix mistakes)		
			$('#letterId' + (cursorPosition - 1)).removeClass();
			$('#cursor' + cursorPosition).html('');
			$('#cursor' + (cursorPosition - 1)).html('|');
			
			cursorPosition--;	
		}
	});
});        

        