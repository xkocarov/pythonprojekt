from website import create_app

app = create_app()

if __name__ == '__main__':      #can be deleted after debugging
    app.run(debug=True)
