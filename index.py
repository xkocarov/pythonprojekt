from flask import Flask, request, render_template, session
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.sql import func
from datetime import datetime
import time
import os

app = Flask(__name__)
app.secret_key = "fjdsahgsadf" # key to encrypt sessiondata
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///myDatabase.sqlite3'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False # getting rid of some warnings

texts = {}   
loadedText = ""       
db = SQLAlchemy(app)

# database model for users
class users(db.Model):
    _userId = db.Column("userId", db.Integer, primary_key = True)
    username = db.Column(db.String(50))
    password = db.Column(db.String(200))
    email = db.Column(db.String(100))
    
    def __init__(self, username, password, email):
        self.username = username
        self.password = password
        self.email = email

# database model for exercises      
class exercises(db.Model):
    _exerciseId = db.Column("exerciseId", db.Integer, primary_key = True)
    userId = db.Column(db.Integer) # foreign key to users table
    lesson = db.Column(db.String(50))
    time = db.Column(db.Float) # exercise time in seconds
    numberOfMistakes = db.Column(db.Integer)
    charactersPerMinute = db.Column(db.Integer)
    exerciseDateTime = db.Column(db.DateTime) #nepouzivam nikde 
    
    def __init__(self, userId, lesson, time, numberOfMistakes, charactersPerMinute, exerciseDateTime):
        self.userId = userId
        self.lesson = lesson
        self.time = time    
        self.numberOfMistakes = numberOfMistakes
        self.charactersPerMinute = charactersPerMinute
        self.exerciseDateTime = exerciseDateTime

# method takes input text and wraps each letter by span with id
# for example:
#   input: asdf
#   output: <span id="letterId0">a</span><span id="letterId1">s</span><span id="letterId2">d</span><span id="letterId3">f</span>
def wrapBySpans(inputText):
    textSeSpanama = ""        
    for i in range(0, len(inputText)):
        textSeSpanama += "<span id=\"cursor" + str(i) + "\"></span><span id=\"letterId" + str(i) + "\">" + inputText[i] + "</span>"
    return textSeSpanama         

# loads all files from folder static/texts    
def loadTextsFromFolder():
    adresar = "static/texts"
    soubory = [i for i in os.listdir(adresar)]
    for soubor in soubory:    
        with open(os.path.join(adresar,soubor)) as file_object:
            texts[soubor] = file_object.read()
            
    ''' vypisuji nactene soubory abych si overila ze se spravne nacetly
    for i in texts:
        print (i, texts[i])  '''     
      
# this function gets letter (for example f) and returns finger (for example levyUkazovacek)      
def getFingerByLetter(letter):      
    return {
        'q': "levyMalicek",
        'a': "levyMalicek",
        'z': "levyMalicek",
        'w': "levyPrstenicek",
        's': "levyPrstenicek",
        'x': "levyPrstenicek",
        'e': "levyProstrednicek",
        'd': "levyProstrednicek",
        'c': "levyProstrednicek",
        'r': "levyUkazovacek",
        'f': "levyUkazovacek",
        'v': "levyUkazovacek",
        't': "levyUkazovacek",
        'g': "levyUkazovacek",
        'b': "levyUkazovacek",
        'y': "pravyUkazovacek",
        'h': "pravyUkazovacek",
        'n': "pravyUkazovacek",
        'u': "pravyUkazovacek",
        'j': "pravyUkazovacek",
        'm': "pravyUkazovacek",
        'i': "pravyProstrednicek",
        'k': "pravyProstrednicek",
        ',': "pravyProstrednicek",
        'o': "pravyPrstenicek",
        'l': "pravyPrstenicek",
        '.': "pravyPrstenicek",
        'p': "pravyMalicek",
        ';': "pravyMalicek",
        '/': "pravyMalicek",
        ' ': "palce"
    }.get(letter, "prstNenalezen")

### PAGE HANDLERS ###

# home page handler
@app.route("/", methods = ['POST', 'GET'])
def home():     
    userMessage = ""
    userError = ""
    loggedUser = ""
    loadTextsFromFolder()
    if request.method == "POST" and request.form["submit"] == "login":
        givenUsername = request.form["username"]
        givenPassword = request.form["password"]
        userInDatabase = users.query.filter_by(username = givenUsername, password = givenPassword).first() # RADEK ODPOVIDA SELECT * FROM user WHERE uderName = givenUsername AND Password - givenPassword
        if userInDatabase:
            session["user"] = givenUsername    #na serveru jestli jsi prihlasen nebo ne je ulozenne v session, nejsem prihlasena, pokud v tom session nic neni.
            userMessage = "Login successful!"                    
        else:
            userError = "Username or password is incorrect!"        
    if "user" in session: # musi to byt v ifu, protoze jinak to nebude fungovat kdyz v tom session neni promenna users. kdyz uzivatel nenei prihlasen
        loggedUser = session["user"] #musim nastavit promennou loggedUser aby se spravne zobrazovala tlacitka v te horni liste. 
    return render_template("home.html", texts=texts, userMessage = userMessage, userError = userError, username = loggedUser)

# quickGame page handler
@app.route("/quickGame")
def quickGame():     
    loadTextsFromFolder()    
    return render_template("quickGame.html", texts=texts)

# registartion page handler
@app.route("/registration", methods = ['POST', 'GET'])
def registration():   
    userMessage = ""
    if request.method == "POST" and request.form["submit"] == "register": # cteni postovych metod
        newUser = users(request.form["username"], request.form["password"], request.form["email"])
        db.session.add(newUser)
        db.session.commit()
        userMessage = "New user successfully created!"
    return render_template("registration.html", texts=texts, userMessage=userMessage)
    
# lesson page handler
@app.route("/lesson")
def lesson(): 
    loggedUser = ""
    lesson = request.args.get("lesson") #cteni getovych metod 
    loadedText = texts[lesson]
    print("Loaded text is " + loadedText) #vypis do terminalu 
    text = wrapBySpans(loadedText)
    if "user" in session:
        loggedUser = session["user"]
    return render_template("lesson.html", lessonText = text, username = loggedUser)  

# page handler for debuging database - this prints all data in database for both tables users and excercises
@app.route("/debugDatabase")
def debugDatabase():  
    allUsers = users.query.all() # select * from users 
    allExercises = exercises.query.all()# select * from exercises
    return render_template("debugDatabase.html", allUsers = allUsers, allExercises = allExercises) 
    
# page handler for "myStatistics" page
# it loads statistics from database and passes them to myStatistics template
@app.route("/myStatistics")
def myStatistics():    
    # 1. musim zjistit userId prave prohlazeneho uzivatele protoze to budu potrebovat do dalsich selektu do tabulky exercises
    # userId prihlaseneho uzivatele si ulozim do promenne userIdOfLoggedUser
    # nasledujici radek je vlastne toto: userIdOfLoggedUser = (SELECT userId FROM users WHERE username = session["user"])
    userIdOfLoggedUser = users.query.filter_by(username = session["user"]).first()._userId
    
    # 2. nasleduji jednotlive selecty z tabulky exercises pro zjisteni jednotlivych statistik
    # nasledujici radek je vlastne toto: totalNumberOfExcerises = (SELECT COUNT(*) FROM exercises WHERE userId = userIdOfLoggedUser)
    totalNumberOfExcerises = exercises.query.filter_by(userId = userIdOfLoggedUser).count()
    
    # nasledujici radek je vlastne toto: averageNumberOfMistakes = (SELECT AVG(numberOfMistakes) FROM exercises WHERE userId = userIdOfLoggedUser)
    averageNumberOfMistakes = exercises.query.with_entities(func.avg(exercises.numberOfMistakes)).filter_by(userId = userIdOfLoggedUser).all()[0][0]
    
    # nasledujici radek je vlastne toto: averageCharactersPerMinute = (SELECT AVG(charactersPerMinute) FROM exercises WHERE userId = userIdOfLoggedUser)
    averageCharactersPerMinute = exercises.query.with_entities(func.avg(exercises.charactersPerMinute)).filter_by(userId = userIdOfLoggedUser).all()[0][0]
    
    # nasledujici radek je vlastne toto: bestSpeed = (SELECT MAX(charactersPerMinute) FROM exercises WHERE userId = userIdOfLoggedUser)
    bestSpeed = exercises.query.with_entities(func.max(exercises.charactersPerMinute)).filter_by(userId = userIdOfLoggedUser).all()[0][0]
    
    # nasledujici radek je vlastne toto: worstSpeed = (SELECT MIN(charactersPerMinute) FROM exercises WHERE userId = userIdOfLoggedUser)
    worstSpeed = exercises.query.with_entities(func.min(exercises.charactersPerMinute)).filter_by(userId = userIdOfLoggedUser).all()[0][0]
    
    # nasledujici radek je vlastne toto: mostPlayedLesson = (SELECT lesson FROM exercises WHERE userId = userIdOfLoggedUser GROUP BY lesson ORDER BY COUNT(lesson) DESC LIMIT 1)    
    mostPlayedLesson = exercises.query.with_entities(exercises.lesson).filter_by(userId = userIdOfLoggedUser).group_by(exercises.lesson).order_by(func.count(exercises.lesson).desc()).first()[0]
    
    # nasledujici radek je vlastne toto: mostPlayedLessonCount = (SELECT COUNT(lesson) FROM exercises WHERE userId = userIdOfLoggedUser GROUP BY lesson ORDER BY COUNT(lesson) DESC LIMIT 1)    
    mostPlayedLessonCount = exercises.query.with_entities(func.count(exercises.lesson)).filter_by(userId = userIdOfLoggedUser).group_by(exercises.lesson).order_by(func.count(exercises.lesson).desc()).first()[0]
    
    # nasledujici radek je vlastne toto: leastPlayedLesson = (SELECT lesson FROM exercises WHERE userId = userIdOfLoggedUser GROUP BY lesson ORDER BY COUNT(lesson) ASC LIMIT 1)    
    leastPlayedLesson = exercises.query.with_entities(exercises.lesson).filter_by(userId = userIdOfLoggedUser).group_by(exercises.lesson).order_by(func.count(exercises.lesson).asc()).first()[0]
    
    # nasledujici radek je vlastne toto: leastPlayedLessonCount = (SELECT COUNT(lesson) FROM exercises WHERE userId = userIdOfLoggedUser GROUP BY lesson ORDER BY COUNT(lesson) ASC LIMIT 1)    
    leastPlayedLessonCount = exercises.query.with_entities(func.count(exercises.lesson)).filter_by(userId = userIdOfLoggedUser).group_by(exercises.lesson).order_by(func.count(exercises.lesson).asc()).first()[0]
    
    # ulozeni username prihlaseneho uzivatale do promenne loggedUser z session (kvuli hornimu menu)
    if "user" in session:
        loggedUser = session["user"]     
        
    # zavolani funkce render_template, chci vyrenderovat "myStatistics.html" a predavam tomu vsechny prave nactene hodnoty
    return render_template("myStatistics.html", 
        username = loggedUser, 
        totalNumberOfExcerises = totalNumberOfExcerises, 
        averageNumberOfMistakes = round(averageNumberOfMistakes, 2),
        averageCharactersPerMinute = round(averageCharactersPerMinute, 2),
        bestSpeed = round(bestSpeed, 2),
        worstSpeed = round(worstSpeed, 2),
        mostPlayedLesson = mostPlayedLesson,
        leastPlayedLesson = leastPlayedLesson,
        mostPlayedLessonCount = mostPlayedLessonCount,
        leastPlayedLessonCount = leastPlayedLessonCount) 

# page handler for logout - this method deletes session
@app.route("/logout")
def logout():     
    session.pop("user", None)
    return render_template("logout.html")  

### END OF PAGE HANDLERS ###

### AJAX HANDLERS ###

# handler for ajax call checkLetter
# returns 3 fields: response (ok / error), nextFinger (for example levyMalicek) and finished (true / false)
@app.route("/checkLetter")
def checkLetterAndReturnNextFingere():
    pismeno = chr(int(request.args.get("letterCode")))
    position = int(request.args.get("position")) #pozice jak moc dalekojsem v tom textu/        
    loadedText = texts[request.args.get("lesson")] #nactu si text na klici ve slovniku texts. 
    response = ""
    finished = False
    if len(loadedText) > position + 1:
        nextFinger = getFingerByLetter(loadedText[position + 1])
    else:
        nextFinger = "prstNenalezen"    
        finished = True
    if loadedText[position] == pismeno:
        response = "ok"
    else:
        response = "error"
    output = {
        "response": response,
        "nextFinger": nextFinger,
        "finished": finished
    }
    return output

# handler for ajax call recordExercise
# records excercise details (userId, time, lesson, ...) to excercises table when excercise is finished
@app.route("/recordExercise")
def recordExercise():     
    # nasledujici radek je vlastne toto: userIdOfLoggedUser = (SELECT userId FROM users WHERE username = session["user"])
    userIdOfLoggedUser = users.query.filter_by(username = session["user"]).first()._userId
    lesson = request.args.get("lesson")
    timeInSeconds = request.args.get("timeInSeconds")
    numberOfMistakes = request.args.get("numberOfMistakes")
    charactersPerMinute = request.args.get("charactersPerMinute")
    newExercise = exercises(userIdOfLoggedUser, lesson, timeInSeconds, numberOfMistakes, charactersPerMinute, datetime.now())
    db.session.add(newExercise)
    db.session.commit()
    return "exerciseSaved"

### END OF AJAX HANDLERS ###

if __name__ == "__main__":
    db.create_all() # vytvori ty tabulky.
    app.run(debug=True)    
