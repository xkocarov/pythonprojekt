from flask import Blueprint, render_template, request, flash, jsonify
from flask_login import current_user
from . import db
from . import texts
from .models import Note

views = Blueprint('views', __name__)
import json


@views.route('/',methods=['GET', 'POST']) # prefix
def home():
    if request.method == 'POST':
        note = request.form.get('note')

        if len(note) <1:
            flash('Note is too short!', category='error')
        else:
            new_note = Note(data=note, user_id=current_user.id)
            db.session.add(new_note)
            db.session.commit()
            flash('Note added!', category='success')

    return render_template("home.html", user=current_user, testovaciText=span_wrap(texts.get('text1')))


@views.route('/delete-note', methods=['POST'])
def delete_note():
    note = json.loads(request.data)
    noteId = note['noteId']
    note = Note.query.get(noteId)
    if note:
        if note.user_id == current_user.id:
            db.session.delete(note)
            db.session.commit()
    return jsonify({})

#speed typing "stroj"

def span_wrap(input_text):
    textwithspans = ""
    for i in range(0, len(input_text)):
        textwithspans += "<span id=\"kurzor" + str(i) + "\"></span><span id=\"letterId" + str(i)+ "\">" + input_text[i] + "</span>"
    return textwithspans

@views.route("/right_letter", methods = ['GET'])
def right_letter():
    pismeno = chr(int(request.args.get("pismeno")))
    pozice = int(request.args.get("pozice"))
    print("na pozici je: " + str(pozice))
    if texts.get('text1')[pozice] == pismeno:
        return "ok"
    else:
        return "error"