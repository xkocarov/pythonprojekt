from flask import Flask
from flask_login import LoginManager
from flask_sqlalchemy import SQLAlchemy
from os import path

db = SQLAlchemy()
DB_NAME = "database.db"

texts = {}


def create_app():
    app = Flask(__name__)
    app.config['SECRET_KEY'] = 'mfcwrogwipnpiwbwghbgs4665' #pouzivame k sifrovani citlivych uzivateslkych dat
    app.config['SQLALCHEMY_DATABASE_URI'] = f'sqlite:///{DB_NAME}'
    db.init_app(app)


    from .views import views
    from .auth import auth

    app.register_blueprint(views, url_prefix='/') #blueprint je "forma" nagenerovani "sekci" ve webove aplikaci
    app.register_blueprint(auth, url_prefix='/')

    from .models import User, Note
    #tecka, delame relativni import, tedy import z nasi vlastni slozky.

    create_database(app)
    initialize_text()

    login_manager = LoginManager()
    login_manager.login_view = 'auth.login'
    login_manager.init_app(app)

    @login_manager.user_loader
    def load_user(id):
        return User.query.get(int(id))

    return app


def create_database(app):
    if not path.exists('website/' + DB_NAME):
        db.create_all(app=app)
        print('Created Database!')
    """ke globalnimu objektu config pristupovat jako ke slovniku. Kazda aplikace flasku ma config objekt."""


def add_text_to_dict(new_text, name):
    texts[name] = new_text


def read_file(filepath, name):
    with open(filepath, "r") as file:
        text = file.read()
        add_text_to_dict(text, name)


def initialize_text():
    read_file("website/text1", "text1")
    read_file("website/text2", "text2")
    read_file("website/text3", "text3")
    read_file("website/text4", "text4")
    read_file("website/text5", "text5")
    #not added to git yet
