from flask import Blueprint, render_template, request, flash, redirect, url_for
from . import db
from website.models import User
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import login_user, login_required, logout_user, current_user

auth = Blueprint('auth', __name__)

@auth.route('/del/all')    # view to delete all users from a database, debbuging only
def del_all_users():
    db.session.query(User).delete()
    db.session.commit()
    return render_template("signup.html", user=current_user)

@auth.route('/del/<id>')    # view to delete single user from a database, debbuging only
def del_user(id):
    user = User.query.filter_by(id=id).first()
    db.session.delete(user)
    db.session.commit()
    return render_template("db_contents.html", users=User.query.all())

@auth.route('/show/all')    # view to show users in a database, debbuging only
def show_users():
    return render_template("db_contents.html", users=User.query.all())

@auth.route('/show/<id>')     # view to show a single user in a database, debbuging only
def show_user(id):
    user = User.query.filter_by(id=id).first()
    return render_template("db_user.html", user=user)


@auth.route('/login', methods=['GET',
                               'POST'])  # URL string v okne vyhledavace. Pokud chceme navstivit stranku bez navvigace skrze mainpage.
def login():
    if request.method == 'POST':
        email = request.form.get('email')
        password = request.form.get('password')
        user = User.query.filter_by(email=email).first()
        if user:
            if check_password_hash(user.password, password):
                flash('Logged in successfuly!', category='success')
                login_user(user, remember=True)
                return redirect(url_for('views.home'))
            else:
                flash('Incorrect password, try again', category='error')
        else:
            flash('Email does not exit', category='error')

    return render_template("login.html", user=current_user)


"""funkce flash fraci none, je to jako void, tato funkce nema navratovou hodnotu
                a potrebuje dva parametry, parametr kategorie specifikuje jaky se ma na tuto message pouzit html ci css styl (mame dve kategorie)"""


@auth.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('auth.login'))
    return "<p>Logout</p>"


@auth.route('/sign-up', methods=['GET', 'POST'])
def sign_up():
    if request.method == 'POST':
        email = request.form.get('email')
        first_name = request.form.get('first_name')
        password1 = request.form.get('password1')
        password2 = request.form.get('password2')

        user = User.query.filter_by(email=email).first()

        if user:
            flash('Email already exist.', category='error')
        elif len(email) < 4:
            flash('Email must be greater than 4 characters.',
                  category='error')  # you can name those categories however u want we gonna use these categories to display messages in different colour.
        elif len(first_name) < 2:
            flash('FirstName must be greater than 1 characters.', category='error')
        elif password1 != password2:
            flash('Passwords don\'t match.', category='error')
        elif len(password1) < 7:
            flash('Password is too short.', category='error')
        else:
            new_user = User(email=email,first_name=first_name,password=generate_password_hash(password1, method='sha256'))
            db.session.add(new_user)
            db.session.commit()
            login_user(new_user, remember=True)
            flash('Account created!', category='success')
            return redirect(url_for('views.home'))
    return render_template("signup.html", user=current_user)  # promennou davame do signup html


""" user = current user /-> preda mi to aktualniho usera z flask loginu do templates/ strance predam promennou user=current_use. 
current user -> predtim se sem ulozi aktualni user, aktualni prihlaeny user, ma to atribut, ze neni autentizovanej. Chceme aby se nam skril login a signup kdyz je autentizovanej"""
